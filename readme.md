#OpenUp - Readme#
As part of the Emotional Resilience project proposed by Dr Laura Biggart and Dr Kamena Henshaw, UEA School of Psychology for the UEA App Challenge 2017.

#### Download OpenUp - latest version v1.0.1 ####
[Download](https://bitbucket.org/ueaseg/uea-emotional-resilience-app/downloads/OpenUp-Proto_v1-0-1.apk)

#### OpenUp - Quick Start Documentation ####
[View Quick Start Documentation](https://bitbucket.org/ueaseg/uea-emotional-resilience-app/downloads/OpenUp-QuickStart-Doc-v1-0-1.pdf)

#### With Thanks To: ####
Developed in collaboration with UEA School of Computing Sciences, Norwich Business School and UEA Research and Innovation Services.

**Developers**

* Conner Reynolds
* Joshua Dennis
* Michael Meyne
* Warren Reynolds

**Development Support**

* Adam Ziolkowski
* Joost Noppen
* Steve Jones

**Project Support**

* Claire Bushell
* Chris Blincoe

**Special Thanks**

* CMP Support (Russ, Matt and Binoop)
* Dom Davis
* Mel Pullen

![UEA PSY](https://pbs.twimg.com/profile_images/875703125956583427/6SzAfPi2.jpg)