function loadMaps(services)
{
  for(i = 0; i < services.length; i++)
  {
    var current = 'mapid' + i; //Current map

    //UEA Lat Long
    var ueaLat = 52.6219;
    var ueaLong = 1.2392;

    //Union Lat long
    var unionLat = 52.621494;
    var unionLong = 1.241527;

    //Current Service Lat long - set to uea lat long if empty
    var currLat =   services[i].lat || unionLat;
    var currLong =  services[i].long || unionLong;

    //create map
    var map = L.map(current, {zoomControl : false});

    //Define mapIcon settings
    var mapIcon = L.Icon.extend({
      options: {
        shadowUrl: 'img/marker-shadow.png',
        iconSize:     [38, 58], // size of the icon
        shadowSize:   [50, 64], // size of the shadow
        iconAnchor:   [19, 58], // point of the icon which will correspond to marker's location
        shadowAnchor: [16, 60],  // the same for the shadow
        popupAnchor:  [-1, -55] // point from which the popup should open relative to the iconAnchor
    }});

    //Create custom markers for different services
    var academicIcon = new mapIcon({iconUrl : 'img/markers/academicMarker.png'}),
      careerIcon = new mapIcon({iconUrl : 'img/markers/careerMarker.png'})
      crisisIcon = new mapIcon({iconUrl : 'img/markers/crisisMarker.png'})
      //faithIcon = new mapIcon({iconUrl : 'img/faithMarker.png'}),
      financeIcon = new mapIcon({iconUrl : 'img/markers/financeMarker.png'}),
      housingIcon = new mapIcon({iconUrl : 'img/markers/housingMarker.png'}),
      internationalIcon = new mapIcon({iconUrl : 'img/markers/internationalMarker.png'}),
      mentalHealthIcon = new mapIcon({iconUrl : 'img/markers/mentalHealthMarker.png'}),
      physicalHealthIcon = new mapIcon({iconUrl : 'img/markers/physicalHealthMarker.png'}),
      socialIcon = new mapIcon({iconUrl : 'img/markers/socialMarker.png'}),
      substanceAbuseIcon = new mapIcon({iconUrl : 'img/markers/substanceAbuseMarker.png'});

    //If current service is in union building
    if(currLat == unionLat && currLong == unionLong)
    {
      //center union building
      currLat = 52.621494;
      currLong = 1.241527;

      //Create Union building area
      var unionPoly = L.polygon([
        [52.62179973, 1.2414567],
        [52.62149, 1.24205],
        [52.62118, 1.24162],
        [52.62148635, 1.2410183]
      ]);

      //Add the union polygon to the map
      unionPoly.addTo(map);
    }
    //Apply custom marker depending on the service catagory
    switch(services[i].Category){
      case 'Academic'://
        var marker = L.marker([currLat, currLong], {icon: academicIcon}).addTo(map);
        break;
      case 'Careers'://
        var marker = L.marker([currLat, currLong], {icon: careerIcon}).addTo(map);
        break;
      case 'Crisis'://
        var marker = L.marker([currLat, currLong], {icon: crisisIcon}).addTo(map);
        break;
      /*case 'Faith':
      L.marker([currLat, currLong], {icon: faithIcon}).addTo(map);
      break;*/
      case 'Finance'://
        var marker = L.marker([currLat, currLong], {icon: financeIcon}).addTo(map);
        break;
      case 'Housing'://
        var marker = L.marker([currLat, currLong], {icon: housingIcon}).addTo(map);
        break;
      case 'International'://
        var marker = L.marker([currLat, currLong], {icon: internationalIcon}).addTo(map);
        break;
      case 'Mental Health':
        var marker = L.marker([currLat, currLong], {icon: mentalHealthIcon}).addTo(map);
        break;
      case 'Physical Health':
        var marker = L.marker([currLat, currLong], {icon: physicalHealthIcon}).addTo(map);
        break;
      case 'Social':
        var marker = L.marker([currLat, currLong], {icon: socialIcon}).addTo(map);
        break;
      case 'Substance Abuse':
        var marker = L.marker([currLat, currLong], {icon: substanceAbuseIcon}).addTo(map);
        break;
    }
    marker.bindPopup("<b>" + services[i].Name + "</b><br>" + services[i].Location).openPopup();


    map.setView([currLat, currLong], 18);

    // add an OpenStreetMap tile layer
   L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
       attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
   }).addTo(map);
  }
}

/**
 * Function to
 * @param  {[type]} lat  [description]
 * @param  {[type]} long [description]
 * @return {[type]}      [description]
 */
function loadLargeMap(lat, long) //Defaults to UEA location, if starting position not given by service in support
{

  mapFunc = function(services){

  //var services = getServices();
  if(!lat || !long)
  {
    lat = 52.62217;  //Default UEA Latitude
    long = 1.24085;  //Default UEA Longitude
  }

  //create map
  var map = L.map('pageMap', {
    center : [lat, long],
    zoom : 17,
    zoomControl : false
  });
  //map.setView([lat, long], 15);

  L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
  }).addTo(map);

  var onUnionClick = function(event)
  {

    showHide('union');
    var concat = "<b><h4>UEA Student Union</h4></b>";
    for(i = 0; i < services.length; i++)
    {
      if(inUnion(services[i][8], services[i][9]))
      {
        concat += services[i][1];
        concat += '<br>';
      }
    }
    document.getElementById('union').innerHTML = concat;
  }

  //Create Union building area
  var unionPoly = L.polygon([
    [52.62179973, 1.2414567],
    [52.62149, 1.24205],
    [52.62118, 1.24162],
    [52.62148635, 1.2410183]
  ], {});

  unionPoly.on('click', onUnionClick);
  unionPoly.addTo(map);
  unionPoly.bindPopup("<b>UEA Student Union</b>");

  var mapIcon = L.Icon.extend({
    options: {
      shadowUrl: 'img/marker-shadow.png',
      iconSize:     [38, 58], // size of the icon
      shadowSize:   [50, 64], // size of the shadow
      iconAnchor:   [19, 58], // point of the icon which will correspond to marker's location
      shadowAnchor: [16, 60],  // the same for the shadow
      popupAnchor:  [-1, -55] // point from which the popup should open relative to the iconAnchor
  }});

  var academicServices = L.layerGroup(),
      careerServices = L.layerGroup(),
      crisisServices = L.layerGroup(),
      financeServices = L.layerGroup(),
      housingServices = L.layerGroup(),
      internationalServices = L.layerGroup(),
      mentalHealthServices = L.layerGroup(),
      physicalHealthServices = L.layerGroup(),
      socialServices = L.layerGroup(),
      substanceAbuseServices = L.layerGroup(),
      faithServices = L.layerGroup();

  var mapOverlay = {
    "Academic" : academicServices,
    "Careers" : careerServices,
    "Crisis" : crisisServices,
    "Finance" : financeServices,
    "Housing" : housingServices,
    "International" : internationalServices,
    "Mental Health" : mentalHealthServices,
    "Physical Health" : physicalHealthServices,
    "Social" : socialServices,
    "Substance Abuse" : substanceAbuseServices,
    "Faith": faithServices,
  }

  var academicIcon = new mapIcon({iconUrl : 'img/markers/academicMarker.png'}),
      careerIcon = new mapIcon({iconUrl : 'img/markers/careerMarker.png'}) //NO CAREER SERVICES YET
      crisisIcon = new mapIcon({iconUrl : 'img/markers/crisisMarker.png'})
      faithIcon = new mapIcon({iconUrl : 'img/markers/faithMarker.png'}),
      financeIcon = new mapIcon({iconUrl : 'img/markers/financeMarker.png'}),
      housingIcon = new mapIcon({iconUrl : 'img/markers/housingMarker.png'}),
      internationalIcon = new mapIcon({iconUrl : 'img/markers/internationalMarker.png'}),
      mentalHealthIcon = new mapIcon({iconUrl : 'img/markers/mentalHealthMarker.png'}),
      physicalHealthIcon = new mapIcon({iconUrl : 'img/markers/physicalHealthMarker.png'}),
      socialIcon = new mapIcon({iconUrl : 'img/markers/socialMarker.png'}),
      substanceAbuseIcon = new mapIcon({iconUrl : 'img/markers/substanceAbuseMarker.png'});

  for(i = 0; i < services.length; i++)
  {

    substring = "SU";

    if( !services[i][8] || !services[i][9])
    {
      services[i].lat = 52.621497; //SU UEA Latitude
      services[i].long = 1.241512; //SU UEA Longitude
      //if(services[i].Name.indexOf(substring) !== -1){
      var servString = "<b>" + services[i][1] + "</b><br>" + servString;

    }
    else{
      var currLat = services[i][8];
      var currLong = services[i][9];


      var cType;

      if (services[i][10].indexOf(",") !== -1){
        cType = services[i][10].split(",")[0].trim()
      } else {
        cType = services[i][10].trim()
      }

      switch(cType){
        case 'Academic'://
            var marker = L.marker([currLat, currLong], {icon: academicIcon}).addTo(map);
            academicServices = L.layerGroup().addLayer(marker);
            break;
        case 'Careers'://
            var marker = L.marker([currLat, currLong], {icon: careerIcon}).addTo(map);
            careerServices = L.layerGroup().addLayer(marker);
            break;
        case 'Crisis'://
            var marker = L.marker([currLat, currLong], {icon: crisisIcon}).addTo(map);
            crisisServices = L.layerGroup().addLayer(marker);
            break;
        case 'Faith':
            var marker = L.marker([currLat, currLong], {icon: faithIcon}).addTo(map);
            faithServices = L.layerGroup().addLayer(marker);
            break;
        case 'Finance'://
            var marker = L.marker([currLat, currLong], {icon: financeIcon}).addTo(map);
            financeServices = L.layerGroup().addLayer(marker);
            break;
        case 'Housing'://
            var marker = L.marker([currLat, currLong], {icon: housingIcon}).addTo(map);
            housingServices = L.layerGroup().addLayer(marker);
            break;
        case 'International'://
            var marker = L.marker([currLat, currLong], {icon: internationalIcon}).addTo(map);
            internationalServices = L.layerGroup().addLayer(marker);
            break;
        case 'Mental Health':
            var marker = L.marker([currLat, currLong], {icon: mentalHealthIcon}).addTo(map);
            mentalHealthServices = L.layerGroup().addLayer(marker);
            break;
        case 'Physical Health':
            var marker = L.marker([currLat, currLong], {icon: physicalHealthIcon}).addTo(map);
            physicalHealthServices = L.layerGroup().addLayer(marker);
            break;
        case 'Social':
            var marker = L.marker([currLat, currLong], {icon: socialIcon}).addTo(map);
            socialServices = L.layerGroup().addLayer(marker);
            break;
        case 'Substance Abuse':
            var marker = L.marker([currLat, currLong], {icon: substanceAbuseIcon}).addTo(map);
            substanceAbuseServices = L.layerGroup().addLayer(marker);
            break;
          }
          marker.bindPopup("<b>" + services[i][1] +"</b><br>" + services[i][5]).openPopup();
      }
    //Switch to determince which icon to create from service catagory
    //}


  }
}
getServiceJSON(mapFunc);
  //layerControl = L.control.layers(null, mapOverlay).addTo(map);
}

/**
 * Function to determine if a given lat and long resides within the Student Union
 * @param  {float} lat  Float representing a latitude
 * @param  {float} long Float representing a longitude
 * @return {boolean}  Boolean determining if service's lat + long resides in the Union
 */
function inUnion(lat, long)
{
  var suLat = 52.621505;
  var suLong = 1.241504;

  if(lat == suLat && long == suLong) return true;
  else return false;
}

function tryMaps() {

  if (foundServices() == 1){
    loadLargeMap('', '');
  } else {
    noServices();
  }
}
