/*****************************************
*
*
*                  SERVICES
*
*****************************************/

var services;

// Entry point for the services page that is called when the device ready
// event fires
function servicesEntryPoint(){

  // Fetch the list of services

  tryServices(function(servicesArray){

    // Asign the fetched list of services to the global services variable
    services = servicesArray;

    // Call the method that will decide how to render the services.
    initSupport();

  });

}

var temp;
var toFilter = []; //Filter List
//var services;
var tmpSelected;
var tmpIndex;

//runs when loading support page, initialises all support service functions
function initSupport(){

  if (window.location.search.substring(1)){

    var d = new Date();
    var n = d.getHours();

    if(n <=8|| n >= 20){
      services = services.filter(function(service){ return service[0] == 1001 });
    } else {
      services = services.filter(function(service){ return service[0] == 1002 });
    }

    displayServices(services);

  } else if(window.location.hash){

    var hash = window.location.hash;
    var split = hash.split('/');
    var arr = split[0].split(',');
    arr[0] = arr[0].replace('#','');
    var ind = split[split.length - 1];
    passDataFlow(arr, ind);
    window.location.hash = '';

  } else {

    //var services = readJson('services.JSON');
    services = sortByName(services, 'upper');
    displayServices(services);
    /*
    services = importJson();

    services = sortByName(services, 'upper');

    displayServices(services);*/
  }

}

function findService(id){

  return services.filter(function(element){return element[0] == id})[0];

}

//sorts the passed array/list, 's', to sort by name in uppercase or lowercase
function sortByName(s, c){

  s.sort(function(a, b){

    if(c == 'upper'){

      if(checkName(a).toUpperCase() < checkName(b).toUpperCase()) return -1;
      if(checkName(a).toUpperCase() > checkName(b).toUpperCase()) return 1;

    } else {

      if(checkName(a) < checkName(b)) return -1;
      if(checkName(a) > checkName(b)) return 1;

    }
  });

  return s;

}

//takes a string, 'str', and removes the string 'remove' if present, returns str
function removeFromString(str, remove){

  var regex = new RegExp( '\\b' + remove + '\\b');
  var test = regex.test(str);

  if(test) str = str.replace(remove,"");

  return str;

}

//checks if passed array contains a certain element, if so returns true, false otherwise
function contains(arr, elem){

  for(i = 0; i < arr.length; i++){

    return (arr[i] === elem)

  }

  return false;

}


//prints header letters to service cards for alphabetic ordering
function printLetter(name){

  var n = name.charAt(0).toUpperCase();
  var concat;

  if(!temp){

    temp = n;
    concat = '<div class = "letter"><p>' + n + '</p></div>';
    return concat;

  } else {

    if(temp != n){

      temp = n;
      concat = '<div class = "letter"><p>' + n + '</p></div>';
      return concat;
    } else {
      return '';
    }
  }
}

function checkName(service){

  if(reg(service[1], 'UEA')) return service[11];
  if(reg(service[1], 'SU'))  return service[11];
  else return service[1];

}

function reg(word, find){

  return new RegExp('\\b' + find + '\\b', 'i').test(word);

}

//reads through list of services, 's', creates all cards, maps, and details
function displayServices(s){

  var concat;
  var l = s.length;

  for(i = 0; i < l; i++)
  {
    concat  +=  printLetter(checkName(s[i]))
            +   '<div id="c'+i+'" class="card">'
            +   '<span id = "arrow' + i + '" onclick="openCard(\'c' + i + '\',' + l + ',\'' + s[i][1] + '\',' + i + ')" class="chevron_toggleable glyphicon glyphicon-chevron-down"></span>'
            +   '<h1 id = cardHead' + i +'>' + checkName(s[i]) + '<br> <b>' + s[i][10] + '</b></h1>';

    var mapID = 'mapid' + i;

    concat  +=  '<div id = "' + mapID + '" class = "map"></div>';

    if(s[i][2].length != 0)
      concat += '<p>' + s[i][2];
    else
      concat += '<p> No description available';

    var websiteBtn = '';
    var emailBtn = '';
    var callBtn = '';

    //test for - or empty values and assign btn variables as necessary

    //Test the website
    if (s[i][7] == '-' || s[i][7] == '') {
      websiteBtn = '';
    } else {
      url = s[i][7];
      websiteBtn = '<button type="button" class="btn btn-primary btn-cardAction" onclick = "saveAction(\'' + s[i][0] + '\', 2, \'' + url + '\');">Visit Website</button>'
    }

    //Test the email
    if (s[i][4] == '-' || s[i][4] == '') {
      emailBtn = '';
    } else {
      url = 'mailto:' + s[i][4];
      emailBtn = '<button type="button" class="btn btn-primary btn-cardAction" onclick = "saveAction(\'' + s[i][0] + '\', 1, \'' + url + '\');">E-Mail Provider</button>';
    }

    //Test the telephone number
    if (s[i][6] == '-' || s[i][6] == '') {
      callBtn = '';
    } else {
      url = 'tel:' + s[i][6];
      callBtn = '<button type="button" class="btn btn-primary btn-cardAction" onclick = "saveAction(\'' + s[i][0] + '\', 0,  \'' + url + '\');">Call: ' + s[i][6] + '</button>';
    }

    //Concat the info
    concat  +=    websiteBtn
            +     emailBtn
            +     callBtn


    concat += '</p></div>';

    concat  +=  '<style>';

    mapID = '#' + mapID;

    concat  +=  mapID + '{ height: 220px; }'
            +   '</style>';
  }


  concat  +=  '<style> #cardWrap { display:block; } </style>';
  concat = removeFromString(concat, 'undefined');

  document.getElementById("cardWrap").innerHTML = concat;

  loadServiceMaps(s);
}

//open a selected card, close all other cards, scroll and focus on opened card
function openCard(card, l, name, index)
{

  card = '#' + card;
  var arrow = '#arrow' + index;

  //saveCardViewed(name);

  if($(card).hasClass('open'))            //check if card is already open
  {
    $(card).toggleClass('open');          //if so, close
    $(arrow).toggleClass('glyphicon-chevron-down glyphicon-chevron-up');
  }
  else
  {
    //Capture Card Viewed Data
    saveCardViewed(card);

    for(i = 0; i < l; i++)
    {
      var saved = '#c' + i;               //save current card
      var sArrow = '#arrow' + i;          //save current arrow
      if(saved != card)
        if($(saved).hasClass('open'))     //check if any other cards are open
        {
          $(saved).toggleClass('open');   //if so, close them
          $(sArrow).toggleClass('glyphicon-chevron-down glyphicon-chevron-up');
        }
    }

    $(card).toggleClass('open');          //open current card
    $(arrow).toggleClass('glyphicon-chevron-down glyphicon-chevron-up');
    focus(card);
  }

  function focus(card)
  {

    var c = card.replace('#c', '');
    var top = '#cardHead' + c;

    setTimeout(
      function()
      {
        $('html,body').animate({
          scrollTop: $(top).offset().top},'slow');
      }, 200);
  }

}

//add a selected element to the list of toFilter
function addToFilter(elem)
{
    $('#' + elem).toggleClass("selected");  //'Select' element
    if(toFilter.indexOf(elem) === -1) { //toFilter is a GLOBAL variable []
      toFilter.push(elem);
    } else {
      for(i = toFilter.length - 1; i >= 0; i--) {
        if(toFilter[i] == elem){
          toFilter.splice(i, 1);
        }
      }
    }
    toFilter = sortByName(toFilter, '');
    readData("services.json").then(function(data) {
      var services = JSON.parse(data);
      services = sortByName(services, 'upper');
      var filtered = [];
      for(i = 0; i < services.length; i++){ //Loop through all existing services (i)
        for(j = 0; j < toFilter.length; j++) { //Loop through services to filter
          var regex = new RegExp(toFilter[j]);
          var match = regex.test(services[i][10]);
          if(match) {
            if(checkItem(services[i])) {
              filtered.push(services[i]);
            }
          }
        }
      }
      filtered = sortByName(filtered, '');
      if(checkLength())
        displayServices(services);
      else
        displayServices(filtered);
      //checks if list of filters is empty, if so, return true, false otherwise
      function checkLength() {
        if(toFilter.length == 0)
          return true;
        else
          return false;
      }
      function checkItem(service)
      {
        return filtered.indexOf(service) === -1;
      }
    });
}
// function addToFilter(elem)
// {
//   $('#' + elem).toggleClass("selected");  //'Select' element
//
//   if(toFilter.indexOf(elem) === -1)
//     toFilter.push(elem);
//   else
//     for(i = toFilter.length - 1; i >= 0; i--)
//       if(toFilter[i] == elem)
//         toFilter.splice(i, 1);
//
//   toFilter = sortByName(toFilter, '');
//
//   services = sortByName(services, 'upper');
//   var filtered = [];
//
//   for(i = 0; i < services.length; i++)
//     for(j = 0; j < toFilter.length; j++)
//     {
//       var regex = new RegExp(toFilter[j]);
//       var match = regex.test(services[i][10]);
//
//       if(match){
//         if(checkItem(services[i]))
//           filtered.push(services[i]);
//       }
//     }
//
//   filtered = sortByName(filtered, '');
//
//   if(checkLength()) displayServices(services);
//   else displayServices(filtered);
//
//   //checks if list of filters is empty, if so, return true, false otherwise
//   function checkLength()
//   {
//     if(toFilter.length == 0) return true;
//     else return false;
//   }
//
//   function checkItem(service)
//   {
//     return filtered.indexOf(service) === -1;
//   }
// }

//toggle the fade of a passed element to show or hide the element
function showHide(elem)
{
  $("#" + elem).fadeToggle("fast");
}

/*****************************************
*
*
*                  MAPS
*
*****************************************/

function getFirstCategory(categories)
{
  var c = categories.split(",");
  return c[0];
}

//iterate all cards and initialise their maps and co-ordinates
function loadServiceMaps(s)
{
  for(i = 0; i < s.length; i++)
  {
    var mapId = 'mapid' + i;

    //UEA position
    var ueaLat = 52.6219;
    var ueaLong = 1.2392;
    //Union Lat long
    var unionLat = 52.621497;
    var unionLong = 1.241512;

    //Define mapIcon settings
    var mapIcon = L.Icon.extend({
      options: {
        shadowUrl: 'img/marker-shadow.png',
        iconSize:     [38, 58], // size of the icon
        shadowSize:   [50, 64], // size of the shadow
        iconAnchor:   [19, 58], // point of the icon which will correspond to marker's location
        shadowAnchor: [16, 60],  // the same for the shadow
        popupAnchor:  [-1, -55] // point from which the popup should open relative to the iconAnchor
    }});

    //Create custom markers for different services
    var academicIcon = new mapIcon({iconUrl : 'img/markers/academicMarker.png'}),
      careerIcon = new mapIcon({iconUrl : 'img/markers/careerMarker.png'}),
      crisisIcon = new mapIcon({iconUrl : 'img/markers/crisisMarker.png'}),
      faithIcon = new mapIcon({iconUrl : 'img/markers/faithMarker.png'}),
      financeIcon = new mapIcon({iconUrl : 'img/markers/financeMarker.png'}),
      housingIcon = new mapIcon({iconUrl : 'img/markers/housingMarker.png'}),
      internationalIcon = new mapIcon({iconUrl : 'img/markers/internationalMarker.png'}),
      mentalHealthIcon = new mapIcon({iconUrl : 'img/markers/mentalHealthMarker.png'}),
      physicalHealthIcon = new mapIcon({iconUrl : 'img/markers/physicalHealthMarker.png'}),
      socialIcon = new mapIcon({iconUrl : 'img/markers/socialMarker.png'}),
      substanceAbuseIcon = new mapIcon({iconUrl : 'img/markers/substanceAbuseMarker.png'});

      //Current Service Lat long - set to uea SU lat long if empty
    var currLat =   s[i][8] || ueaLat;
    var currLong =  s[i][9] || ueaLong;

    //alert("Name:" + s[i][1] + "\nLat: " + s[i][8] + "\nLong: " + s[i][9]);

    //create map
    var map = L.map(mapId, {zoomControl : false, attributionControl : false});

    ///If the service is in the union
    if(currLat == unionLat && currLong == unionLong)
    {
      var unionPoly = L.polygon([
        [52.62179973, 1.2414567],
        [52.62149, 1.24205],
        [52.62118, 1.24162],
        [52.62148635, 1.2410183]]);
      currLat = 52.621497;
      currLong = 1.241512;
      unionPoly.addTo(map);
    }

    switch(getFirstCategory(s[i][10])){
      case 'Academic'://
        var marker = L.marker([currLat, currLong], {icon: academicIcon}).addTo(map);
        break;
      case 'Careers'://
        var marker = L.marker([currLat, currLong], {icon: careerIcon}).addTo(map);
        break;
      case 'Crisis'://
        var marker = L.marker([currLat, currLong], {icon: crisisIcon}).addTo(map);
        break;
      case 'Faith':
        var marker = L.marker([currLat, currLong], {icon: faithIcon}).addTo(map);
      break;
      case 'Finance':
        var marker = L.marker([currLat, currLong], {icon: financeIcon}).addTo(map);
        break;
      case 'Housing'://
        var marker = L.marker([currLat, currLong], {icon: housingIcon}).addTo(map);
        break;
      case 'International'://
        var marker = L.marker([currLat, currLong], {icon: internationalIcon}).addTo(map);
        break;
      case 'Mental Health':
        var marker = L.marker([currLat, currLong], {icon: mentalHealthIcon}).addTo(map);
        break;
      case 'Physical Health':
        var marker = L.marker([currLat, currLong], {icon: physicalHealthIcon}).addTo(map);
        break;
      case 'Social':
        var marker = L.marker([currLat, currLong], {icon: socialIcon}).addTo(map);
        break;
      case 'Substance Abuse':
        var marker = L.marker([currLat, currLong], {icon: substanceAbuseIcon}).addTo(map);
        break;
    }
    marker.bindPopup("<b>" + s[i][1] + "</b><br>" + s[i][5]).openPopup();
    map.setView([currLat, currLong], 18);
    // add an OpenStreetMap tile layer
   L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
       attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
   }).addTo(map);
  }
}

//initialise the large map with all markers and polygons
function loadLargeMap(lt, lng)
{
  if(lt > 52.9999 || lt < 52.0000)  lt = 52.625122;  //Default UEA Latitude
  if(lng > 1.9999 || lt < 1.0000)   lng = 1.241851;  //Default UEA Longitude

  var map = L.map('pageMap', {
    center : [52.621864, 1.242160],
    zoom : 18,
    attributionControl : false,
    zoomControl : false
  });

  L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
  }).addTo(map);

  var union = drawUnion(lt, lng);
  union.addTo(map);

  union.on('click', function(e){});

  initOverlay(map);
}

//initialise overlays for each category of service
function initOverlay(map)
{

  services = importJson();

  //Define mapIcon settings
  var mapIcon = L.Icon.extend({
    options: {
      shadowUrl: 'img/marker-shadow.png',
      iconSize:     [38, 58], // size of the icon
      shadowSize:   [50, 64], // size of the shadow
      iconAnchor:   [19, 58], // point of the icon which will correspond to marker's location
      shadowAnchor: [16, 60],  // the same for the shadow
      popupAnchor:  [-1, -55] // point from which the popup should open relative to the iconAnchor
  }});

  var academicIcon = new mapIcon({iconUrl : 'img/markers/academicMarker.png'});
  var careerIcon = new mapIcon({iconUrl : 'img/markers/careerMarker.png'}); //NO CAREER SERVICES YET
  var crisisIcon = new mapIcon({iconUrl : 'img/markers/crisisMarker.png'});
  var defaultIcon = new mapIcon({iconUrl : 'img/markers/map_marker-red1.png'});
  var faithIcon = new mapIcon({iconUrl : 'img/markers/faithMarker.png'});
  var financeIcon = new mapIcon({iconUrl : 'img/markers/financeMarker.png'});
  var housingIcon = new mapIcon({iconUrl : 'img/markers/housingMarker.png'});
  var internationalIcon = new mapIcon({iconUrl : 'img/markers/internationalMarker.png'});
  var mentalHealthIcon = new mapIcon({iconUrl : 'img/markers/mentalHealthMarker.png'});
  var physicalHealthIcon = new mapIcon({iconUrl : 'img/markers/physicalHealthMarker.png'});
  var socialIcon = new mapIcon({iconUrl : 'img/markers/socialMarker.png'});
  var sportIcon = new mapIcon({iconUrl : 'img/markers/sportMarker.png'});
  var substanceAbuseIcon = new mapIcon({iconUrl : 'img/markers/substanceAbuseMarker.png'});

  for(i = 0; i < services.length; i++)
  {
    var lt = services[i][8];
    var lng = services[i][9];
    if(!serviceInUnion(services[i])){
      switch (getFirstCategory(services[i][10]))
      {
        case 'Academic':
          var marker = L.marker([lt, lng], {icon: academicIcon}).addTo(map);
          break;

        case 'Careers':
          var marker = L.marker([lt, lng], {icon: careerIcon}).addTo(map);
        break;

        case 'Crisis':
          var marker = L.marker([lt, lng], {icon: crisisIcon}).addTo(map);
        break;

        case 'Faith':
          var marker = L.marker([lt, lng], {icon: faithIcon}).addTo(map);
        break;

        case 'Finance':
          var marker = L.marker([lt, lng], {icon: financeIcon}).addTo(map);
        break;

        case 'Housing':
          var marker = L.marker([lt, lng], {icon: housingIcon}).addTo(map);
        break;

        case 'International':
          var marker = L.marker([lt, lng], {icon: internationalIcon}).addTo(map);
        break;

        case 'Mental Health':
          var marker = L.marker([lt, lng], {icon: mentalHealthIcon}).addTo(map);
        break;

        case 'Physical Health':
          var marker = L.marker([lt, lng], {icon: physicalHealthIcon}).addTo(map);
        break;

        case 'Social':
          var marker = L.marker([lt, lng], {icon: socialIcon}).addTo(map);
        break;

        case 'Sport':
          var marker = L.marker([lt, lng], {icon: sportIcon}).addTo(map);
        break;

        case 'Substance Abuse':
          var marker = L.marker([lt, lng], {icon: substanceAbuseIcon}).addTo(map);
        break;

        default:
          var marker = L.marker([lt, lng], {icon: defaultIcon}).addTo(map);
        break;
      }
      marker.bindPopup("<b>" + services[i][1] +"</b><br>" + services[i][10]).openPopup();
    }
  }
}

//draw a polygon highlighting the union building on UEA campus
function drawUnion(lt, lng)
{
  var modal = document.getElementById('unionModal');
  var span = document.getElementsByClassName("close")[0];

  span.onclick = function() {
    modal.style.display = "none";
  }

  window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
  }


  var unionModal = function()
  {
    var modal = document.getElementById('unionModal');
    var concat;

    for(i = 0; i < services.length; i++)
    {
      if(serviceInUnion(services[i])){
        concat += '<p>' + services[i][11] + '</p>';
        concat += '<br>';
      }
    }

    document.getElementById('unionContent').innerHTML = checkReplace(concat);


    modal.style.display = "block";
  }

  //Create Union building area
  var unionPoly = L.polygon([
    [52.62179973, 1.2414567],
    [52.62149, 1.24205],
    [52.62118, 1.24162],
    [52.62148635, 1.2410183]
  ]).on('click', unionModal)

  return unionPoly;
}

function serviceInUnion(service)
{
  if(service[3] == 'SU' || service[5] == 'Union House')
    return true;
  else return false;
}

//add a map marker to the given coordinates, lt and lng, and apply given attributes
function addMarker(lt, lng, ic, service, name, loc, map)
{
  var marker = L.marker([lt, lng], {icon: ic}).addTo(map);
  service = L.layerGroup().addLayer(marker);
  marker.bindPopup("<b>" + name +"</b><br>" + loc).openPopup();
}

/*****************************************
*
*
*                  FUNCTIONALITY
*
*****************************************/
function passDataFlow(selectedReasons, selectedEmotionId){

  // Get the service to reason/emotion mappings

  var mappings = getFeelingLinks();
  var linkedServices = [];

  // Loop through the mappings

  mappings.forEach(function(mapping){

    var emotionId = mapping[1];

    if(emotionId == selectedEmotionId){

      selectedReasons.forEach(function(selectedReason){

        var mappingReason = mapping[2];
        var mappingServices = mapping[3];

        if (selectedReason == mappingReason){

          mappingServices.forEach(function(service){

            var found = findService(service);

            if (found && !linkedServices.includes(found)){
              linkedServices.push(found);
            }

          });

        }

      });

    }

  });

    console.log("All connected services: ");
    for(i = 0; i < linkedServices.length; i++){
      console.log(linkedServices[i]);
      console.log('');
    }

    linkedServices = sortByName(linkedServices, 'upper');
    displayServices(linkedServices);

}
function initModal(id)
{
  // Get the modal
  var modal = document.getElementById(id);

  // Get the button that opens the modal
  var btn = document.getElementById(id + "Btn");

  // Get the <span> element that closes the modal
  var span = document.getElementsByClassName(id + "Close")[0];

  // When the user clicks the button, open the modal
  btn.onclick = function() {
    modal.style.display = "block";
  }

  // When the user clicks on <span> (x), close the modal
  span.onclick = function() {
      modal.style.display = "none";
  }

  // When the user clicks anywhere outside of the modal, close it
  window.onclick = function(event) {
      if (event.target == modal) {
          modal.style.display = "none";
      }
  }
}

function goTo(url)
{
  window.location.href = url;
}
