// Emoji indexes (Update when emojis are added or removed), used to obtain the
// number representing this feeling.
var emojis = [
  ['angry', 0, 'Anger is a very difficult emotion, finding a way to manage anger is important.  Please have a look at these links for support.'],
  ['anxious', 1, 'When you are feeling anxious it is difficult to get your worries out of your mind – talking can help you think differently, see links'],
  ['flat', 2, 'Sorry to hear you are feeling flat, it would be good to explore why you may be feeling flat.  Please have a look at these links for support'],
  ['frustrated', 3, 'Sorry to hear you are feeling frustrated, getting help with finding solutions can help.  Please have a look at these links for support'],
  ['happy', 4, 'Great to hear that! You may also be interested in these links'],
  ['lonely', 5, 'Sorry you are feeling lonely, many students often feel this way. Try joining a group to find common interests and friendships – see links'],
  ['overwhelmed', 6, 'When you are feeling overwhelmed your problems can seem hopeless but these links may help.'],
  ['sad', 7, 'Sorry to hear you are feeling sad.  Life can be a rollercoaster sometimes – we can help you through this time, take a look at these links'],
  ['scared', 8, 'If you are feeling scared it is very important that you talk to someone and get support.  Please have a look at these links']
];

function getFeelings()
{
  var feelings = [
    [ 'angry',
        'Did not get marks hoping for',
        'Betrayed by partner/friend/family',
        'Acted aggressively'
    ],

    [
      'anxious',
        'Family member ill',
        'Unsure if can cope with studies',
        'Future career plans',
        'Relationship worries',
        'Social anxiety',
        'Lack of social group',
        'Getting a job to earn money',
        'Budgeting my money',
        'Eating problems',
        'Feel like I don’t belong',
        'Confused about my sexuality'
    ],

    [
      'flat',
        'Not able to feel any emotion',
        'Not enjoying life'
    ],

    [
      'frustrated',
        'I cannot access parts of the campus',
        'Messy flatmates',
        'Feeling things are not fair'
    ],

    [
      'happy',
        'Wanting to make new friends',
        'Wanting to learn new study skills',
        'Feeling motivated to try new things'
    ],

    [
      'lonely',
        'Struggling to make new friends',
        'Homesick'
    ],

    [
      'overwhelmed',
        'With independence',
        'With coursework',
        'With socialising'
    ],

    [
      'sad',
        'Family issue',
        'Relationship breakdown',
        'Homesick'
    ],

    [
      'scared',
        'Feeling harassed',
        'Abuse within relationship',
        'Drug and alcohol misuse',
        'Been physically attacked'
    ]
  ];
  return feelings;
}

function getFeelingLinks()
{
  var feelingLinks = [

    // Happy mappings

    [
      "Happy",
       4,
       "Wanting to make new friends",
       [
        1024 // Uea clubs & socs
      ]
    ],
    [
      "Happy",
       4,
       "Wanting to learn new study skills",
       [
        1010 // UEA study skills
      ]
    ],
    [
      "Happy",
       4,
       "Feeling motivated to try new things",
       [
        1024, // Uea clubs & socs
        1035 // Sportspark
      ]
    ],

    // Sad Mappings

    [
      "Sad",
       7,
       "Family issue",
       [
        1003, //"Advisor (UEA)",
        1009, //"Counselling and Mental Health (UEA)",
        1001, //"Nightline (UEA)",
        1021, //"Health and Wellbeing (SU)",
        1002 //"Samaritans confidential helpline(external)"
      ]
    ],
    [
      "Sad",
       7,
       "Relationship breakdown",
       [
        1009, //"Counselling and Mental Health (UEA)",
        1001, //"Nightline (UEA)",
        1021, //"Health and Wellbeing (SU)",
        1002, //"Samaritans confidential helpline(external)",
        1013, //"Harrassment, Bullying, Sexual Violence (UEA)",
        1007, //"Lesbian, Gay, Bisexual and Transgender support - Pride (SU)",
        1027, //"Mental Health charity - MIND (external)",
        1033 // iCaSH "Contraception and Sexual Health NHS (external)"
      ]
    ],
    [
      "Sad",
       7,
       "Homesick",
       [
        1001, // "Nightline",
        1021, // "Health and Wellbeing (SU)",
        1002, // "Samaritans",
        1022, // "Faith and spirituality",
        1034, // "Multifaith centre (UEA)",
        1024, // "Clubs and Societies",
        1014, // "International students (UEA)",
        1023 // "International students (SU)"
      ]
    ],

    // Anxious mappings

    [
      "Anxious",
       1,
       "Family member ill",
       [
        1003, //"Advisor",
        1009, //"Counselling and Mental Health",
        1001, //"Nightline",
        1021, //"Health and Wellbeing (SU)",
        1002, //"Samaritans",
        1022, //"Faith and spirituality (SU)",
        1034 //"Multifaith centre (UEA)"
      ]
    ],
    [
      "Anxious",
       1,
       "Unsure if can cope with studies",
       [
        1003, //"Advisor",
        1017, //"Academic advice (SU)",
        1010, //"UEA Study Skills",
        1014 //"International students (UEA)",
      ]
    ],
    [
      "Anxious",
       1,
       "Future career plans",
       [
        1005, //"Careers centre (UEA)",
        1018 //"Employment advice (SU)"
      ]
    ],
    [
      "Anxious",
       1,
       "Relationship worries",
       [
         1009, //"Counselling and Mental Health (UEA)",
         1001, //"Nightline (UEA)",
         1021, //"Health and Wellbeing (SU)",
         1002, //"Samaritans confidential helpline(external)",
         1013, //"Harrassment, Bullying, Sexual Violence (UEA)",
         1007, //"Lesbian, Gay, Bisexual and Transgender support - Pride (SU)",
         1027, //"Mental Health charity - MIND (external)",
         1033, // iCaSH "Contraception and Sexual Health NHS (external)"
         1034, //"Multifaith (UEA)",
         1022 //"Faith and spirituality (SU)"
      ]
    ],
    [
      "Anxious",
       1,
       "Social anxiety",
       [
         1009, //"Counselling and Mental Health",
         1001, //"Nightline",
         1021, //"Health and Wellbeing (SU)",
         1002 //"Samaritans"
      ]
    ],
    [
      "Anxious",
       1,
       "Lack of a social group",
       [
         1034, //"Multi-faith centre UEA",
         1007, //"UEA Pride",
         1022, //"Faith and spirituality (SU)",
         1024, // Clubs and societies
         1014, // "International students (UEA)",
         1023, // "International students (SU)"
         1004 //Senior Resident
      ]
    ],
    [
      "Anxious",
       1,
       "Getting a job to earn money",
       [
         1005, //"Careers centre (UEA)",
         1018 //"Employment advice (SU)"
      ]
    ],
    [
      "Anxious",
       1,
       "Budgeting my money",
       [
         1008, // Student support services
         1011, // UEA Finance
         1019, // SU Money Advice
         1031 //"Advice – citizens advice bureau (external)"
      ]
    ],
    [
      "Anxious",
       1,
       "Eating Problems",
       [
         1026, // UEA Medical Center
         1029, // B-eat
         1028, // Eating Matters
         1009, // Counselling and mental health
         1001, //"Nightline",
         1021, //"Health and Wellbeing (SU)",
         1030, //"Wellbeing NHS",
         1002 //"Samaritans"
      ]
    ],
    [
      "Anxious",
       1,
       "Feel like I don’t belong",
       [
        1024, //"Clubs and Societies",
        1035, //"Sportspark",
        1007, //"LGBT (SU)",
        1034, //"Multifaith (UEA)",
        1022, //"Faith and spirituality (SU)",
        1014, //"International students (UEA)",
        1023, //"International students (SU)",
        1004 //"Senior Resident"
      ]
    ],
    [
      "Anxious",
       1,
       "Confused about my sexuality",
       [
        1007, //"LGBT (SU)",
        1013, //"Harassment (UEA)",
        1001, //"Nightline",
        1002, //"Samaritans",
        1033, //"Contraception and sexual health"
      ]
    ],

    [
      "Scared ",
       8,
       "Feeling harassed",
       [
        1013, //"Harassment",
        1016, //"Security (UEA)",
        1038, //Police
        1009, //"Counselling and Mental Health (UEA)",
        1026, //"Medical Centre (UEA)",
        1001, //"Nightline",
        1021, //"Health and Wellbeing (SU)",
        1002 //"Samaritans"
      ]
    ],
    [
      "Scared",
       8,
       "Abused within relationship",
       [
        1009, //"Counselling and Mental Health (UEA)",
        1013, //"Harassment (UEA)",
        1001, //"Nightline",
        1021, //"Health and Wellbeing (SU)",
        1002, //"Samaritans"
        1037, //Police Emergency
        1038 //Police (general)
      ]
    ],
    [
      "Scared",
       8,
       "Drug and alcohol misuse",
       [
        1032, //"The Matthew Project",
        1025, //"FRANK",
        1001, //"Nightline",
        1021, //"Health and Wellbeing (SU)",
        1002 //"Samaritans"
      ]
    ],
    [
      "Scared",
       8,
       "Been physically attacked",
       [
        1037, //Police Emergency
        1038, //Police (general)
        1016, //"Security",
        1009, //"Counselling and mental health",
        1001, //"Nightline",
        1021, //"Health and Wellbeing (SU)",
        1002 //"Samaritans"
      ]
    ],
    [
      "Angry",
       0,
       "Did not get marks hoping for",
       [
        1003, //"Advisor",
        1010, //"UEA Study Skills"
        1017 //"Academic advice (SU)",
      ]
    ],
    [
      "Angry",
       0,
       "Betrayed by partner/friend/family",
       [
        1001, //"Nightline",
        1021, //"Health and Wellbeing (SU)",
        1009, //"Counselling and mental health",
        1002 //"Samaritans"
      ]
    ],
    [
      "Angry",
       0,
       "I acted aggressively",
       [
        1001, //"Nightline",
        1009, //"Counselling and mental health",
        1002 //"Samaritans"
      ]
    ],
    [
      "Frustrated",
      3,
      "I cannot access parts of the campus",
      [
        1012, // UEA Disability
        1021, // Health and wellbeing
        1003, // Advisor
        1015 // Accommodation
      ]
    ],
    [
      "Frustrated",
       3,
       "Messy flatmates",
       [
        1004, //"Senior resident",
        1015, //"Accommodation (UEA)",
        1020, //"Housing advice (SU)",
        1013, //"Harassment (UEA)",
        1016 //"Security (UEA)"
      ]
    ],
    [
      "Frustrated",
       3,
       "Feeling things are not fair",
       [
        1003, //"Advisor",
        1010, //"Study Skills (UEA)",
        1017, //"Academic Advice (SU)",
        1004, //"Senior resident",
        1015, //"Accommodation (UEA)",
        1020 //"Housing advice (SU)"
      ]
    ],
    [
      "Flat",
       2,
       "Not able to feel any emotion",
       [
        1026, //"Medical Centre (UEA)",
        1009, //"Counselling and Mental Health",
        1001, //"Nightline",
        1002, //"Samaritans",
        1027 //"MIND"
      ]
    ],
    [
      "Flat",
       2,
       "Not enjoying life",
       [
        1026, //"Medical Centre (UEA)",
        1009, //"Counselling and Mental Health",
        1001, //"Nightline",
        1021, //"Health and Wellbeing (SU)",
        1002, //"Samaritans",
        1027, //"MIND",
        1024, //"Clubs and Societies",
        1035 //"Sportspark"
      ]
    ],
    [
      "Overwhelmed",
       6,
       "With independence",
       [
        1011, //"Finance (UEA)",
        1031, //"Advice – citizens advice bureau (external)",
        1019, //"Money advice (SU)",
        1015, //"Accommodation",
        1010, //"UEA Study Skills",caree
        1001, //"Nightline",
        1024, //"clubs and societies",
        1035 //"Sportspark"
      ]
    ],
    [
      "Overwhelmed",
       6,
       "With coursework",
       [
        1003, //"Advisor",
        1010, //"Study Skills (UEA)",
        1017 //"Academic advice (SU)"
      ]
    ],
    [
      "Overwhelmed",
       6,
       "With having to socialise",
       [
        1024, //"Clubs and Societies",
        1035, //"Sportspark",
        1007, //"LGBT (SU)",
        1034, //"Multifaith (UEA)",
        1022, //"Faith and spirituality (SU)",
        1014, //"International students (UEA)",
        1023, //"International students (SU)",
        1004 //"Senior Resident"
      ]
    ],
    [
      "Lonely ",
       5,
       "Struggling to make new friends",
       [
        1001, //"Nightline",
        1002, // Samaritans
        1024, //"Clubs and societies",
        1035, //"Sportspark",
        1007, //"LGBT",
        1034, //"Multifaith",
        1022 //"Faith and spirituality"
      ]
    ],
    [
      "Lonely",
       5,
       "Homesick",
       [
        1001, //"Nightline",
        1021, //"Health and Wellbeing (SU)",
        1002, //"Samaritans",
        1022, //"Faith and spirituality",
        1034, //"Multifaith centre (UEA)",
        1024, //"Clubs and Societies",
        1014, //"International students (UEA)",
        1023 //"International students (SU)"
      ]
    ]
  ]

  return feelingLinks;
}

var intensity;

///<  Load the options for each emoji when selected
//    param e   -->   the element index of the chosen feeling, used to grab
//                    the choices relevant to the chosen feeling
function loadEmotions(name)
{
  intensity = $('input[type=range]')[0].value;
  var feelings = getFeelings();
  console.log(feelings);
  var concat = "";
  var length = feelings.length;
  var chosen;

  for(i = 0; i < length; i++)
  {
    if(name == feelings[i][0]){
      chosen = i;
      console.log(chosen);
      for(j = 0; j < feelings[i].length; j++){
        if(j != 0){
          concat +=  "<div onclick = 'toggleClass(" + j + ")' id = 'o" + j + "' class = option> <h1>" +
                      feelings[i][j] + "</h1> </div>";
        }
      }
    }

  }
  concat += '<div class = "row">' +
            '<button class="btn btn-primary" id="buttonTwo" onclick="backButton()"><h1>Back</h1></button>' +
            '<button class="btn btn-primary" id="buttonTwo" onclick="clearOptions(' + length + ')"><h1>Clear</h1></button>'  +
            '<button class="btn btn-primary" id="buttonTwo" onclick="getChosen(' + chosen + ')"><h1>Next</h1></button>' +
            '</div>';

  document.getElementById('emojiWrap').innerHTML = concat;
}

/**
 *
 */
function getChosen(index)
{
  var feelings = getFeelings();
  var chosen = feelings[index];
  var selected = [];

  for(i = 0; i < chosen.length; i++)
  {
    if($('#o' + i).hasClass("selected")){
      selected.push(chosen[i]);
    }
  }

  reasonDialogBox(chosen[0], selected, index);
}

function reasonDialogBox(emoji, selected, index){

  console.log(selected);
  console.log(index);

  var hash = '#' + selected + '/' + index;
  saveEmoji(emoji, intensity, selected, "support.html"+ hash);
  for(i = 0; i < emojis.length; i++) if(emojis[i][0] == emoji) navigator.notification.alert(emojis[i][2]);

}

function getSelected()  { return tmpSelected; }
function getIndex()     { return tmpIndex; }

///<  Toggle the display of the passed element on and off
//    param elem  -->   toggle the display value of a passed div element
function showHide(elem)
{
  $("#" + elem).fadeToggle("fast");
}

function toggleClass(i)
{
  $("#o" + i).toggleClass("selected");
}

function clearOptions(length)
{
    for(i = 0; i < length + 1; i++)
    {
      var id = '#o' + i;
      if( $(id).hasClass("selected") )
      {
        console.log(id);
        toggleClass(i);
      }

    }
}

//Changes the size of the emoji images depending on the size of the screen
function pickEmojiSize(){
  var screenWidth = window.screen.availWidth;
  if(screenWidth < 600){
    return 42;
  }else if (screenWidth < 1000) {
    return 82;
  }else{
    return 122;
  }
}

///<  Loads 3x3 grid of selectable emoji feelings
function loadEmojiSelection()
{
  // If emojiSliderPopup has been selected when reverting back to grid this will
  // check and set the visibility of the grid and re-adjust the height styling.
  if(!$("#emojiWrap").is(':visible')){
    $('#emojie-drop').css("height", "unset"); // Resetting height to tightly fit emoji grid.
    $('#main-p').css("margin-bottom", "1%"); // Resetting margin to reduce gap between first row of emojis and p tag.
    showHide("emojiSliderPopup");
    showHide("emojiWrap");

    // Reverting heading to asking how they feel not how intense.
    document.getElementById("main-p").innerHTML = "How are you feeling today?";
  }


  var size = pickEmojiSize();
  var content = "";

  content = '<div class="row emojiRow">';

  //Angry
  content +=  '<a onclick = "loadSlider(\'angry\', 0)" href="#">' +
              '<div class = "col-xs-4 emojiItem"> <div class = "pic">' +
              '<img src = "img/emojis/angry.png" alt = "angry" height = "' + size + '" width = "' + size + '">' +
              '<p class = "emojiName"> Angry </p> </div> </div> </a>'

  //Anxious
  content +=  '<a onclick = "loadSlider(\'anxious\', 1)" href="#">' +
              '<div class = "col-xs-4 emojiItem"> <div class = "pic">' +
              '<img src = "img/emojis/anxious.png" alt = "anxious" height = "' + size + '" width = "' + size + '">' +
              '<p class = "emojiName"> Anxious </p> </div> </div> </a>'

  //Flat
  content +=  '<a onclick = loadSlider(\'flat\', 2)" href="#">' +
              '<div class = "col-xs-4 emojiItem"> <div class = "pic">' +
              '<img src = "img/emojis/flat.png" alt = "flat" height = "' + size + '" width = "' + size + '">' +
              '<p class = "emojiName"> Flat </p> </div> </div> </a> </div>'

  //Frustrated
  content += '<div class="row emojiRow">';

  content +=  '<a onclick = "loadSlider(\'frustrated\', 3)" href="#">' +
              '<div class = "col-xs-4 emojiItem"> <div class = "pic">' +
              '<img src = "img/emojis/frustrated.png" alt = "frustrated" height = "' + size + '" width = "' + size + '">' +
              '<p class = "emojiName"> Frustrated </p> </div> </div> </a>'

  //Happy
  content +=  '<a onclick = "loadSlider(\'happy\', 4)" href="#">' +
              '<div class = "col-xs-4 emojiItem"> <div class = "pic">' +
              '<img src = "img/emojis/happy.png" alt = "happy" height = "' + size + '" width = "' + size + '">' +
              '<p class = "emojiName"> Happy </p> </div> </div> </a>'

  //Lonely
  content +=  '<a onclick = "loadSlider(\'lonely\', 5)" href="#">' +
              '<div class = "col-xs-4 emojiItem"> <div class = "pic">' +
              '<img src = "img/emojis/lonely.png" alt = "Lonely" height = "' + size + '" width = "' + size + '">' +
              '<p class = "emojiName"> Lonely </p> </div> </div> </a> </div>'

  content += '<div class = "row emojiRow">';

  //Overwhelmed
  content +=  '<a onclick = "loadSlider(\'overwhelmed\', 6)" href="#">' +
              '<div class = "col-xs-4 emojiItem"> <div class = "pic">' +
              '<img src = "img/emojis/overwhelmed.png" alt = "overwhelmed" height = "' + size + '" width = "' + size + '">' +
              '<p class = "emojiName"> Overwhelmed </p> </div> </div> </a>';

  //Sad
  content +=  '<a onclick = "loadSlider(\'sad\', 7)" href="#">' +
              '<div class = "col-xs-4 emojiItem"> <div class = "pic">' +
              '<img src = "img/emojis/sad.png" alt = "sad" height = "' + size + '" width = "' + size + '">' +
              '<p class = "emojiName"> Sad </p> </div> </div> </a>';

  //Scared
  content +=  '<a onclick = "loadSlider(\'scared\', 8)" href="#">' +
              '<div class = "col-xs-4 emojiItem"> <div class = "pic">' +
              '<img src = "img/emojis/scared.png" alt = "scared" height = "' + size + '" width = "' + size + '">' +
              '<p class = "emojiName"> Scared </p> </div> </div> </a> </div>';

  document.getElementById('emojiWrap').innerHTML = content;

}

///<  Loads the third panel after a feeling is chosen
//    param elem  -->   the element index of the chosen feeling, used to grab
//                      the choices relevant to the chosen feeling
function loadChoice(){

  // Resolve the name of the emoji from the filename used on the slider image

  var emjoiSliderImageDef = $('.rangeslider__handle').css('background-image');
  var emojiImagePath = emjoiSliderImageDef.substr(5, emjoiSliderImageDef.length - 7);
  var emojiFileName = emojiImagePath.substr(emojiImagePath.lastIndexOf("/") + 1);
  var emojiName = emojiFileName.split(".")[0];

  var intensity = $("#intensity").text().substr($("#intensity").text().indexOf(":") + 2);

  var content = "";
  content   +=  '<div id = "main-p">';

  var emojiDimension = $('#main-p').height(); //Height and width of emoji in header bar.

  content   +=  '<figure> <figcaption>Why do you feel ' + emojiName + '?' + '<img onclick = "loadEmojiSelection()" id = "selected" src = "' + emojiImagePath + '"'  +
  'height =' + emojiDimension + 'width =' + emojiDimension + '/></figcaption> </figure> </div>';


  content   +=  '<div id = "emojiWrap"> </div>';

  content +=    '<div id="emojiSliderPopup">' +
                '<input type="range" min="1" max="5" value="1" data-rangeslider data-orientation="vertical">' +
                '<div id="wrapper">' +
                '<output id="intensity">Slightly: 1</output> </div> <div id="navButtons">' +
                '<script>js/rangeslider.js</script>' +
                '<button onclick="loadEmojiSelection()" class = "btn btn-primary" type="button">Back</button>' +
                '<button onClick="loadChoice()" class = "btn btn-primary" type="button">Next</button> </div> </div>';

  document.getElementById('emojie-drop').innerHTML = content;

  sliderOuputCustomisation(); //Ensures the output tag is being updated.
  loadEmotions(emojiName);

  $('#emojie-drop').css("height", "unset");
  $('#emojie-drop').css("overflow", "scroll");

  document.getElementById('emojie-drop').scrollTop = 0;

}

/*******************************************************************************
 *
 * @param {Number} divisor
 * @return
 ******************************************************************************/
function getSliderContainerScaledHeight(divisor){

  var emojiSliderContainerHeight = $('#emojie-drop').height() - ($('#main-p').height() + parseInt($('#main-p').css("margin-bottom")));
  var scaledHeight = emojiSliderContainerHeight / parseInt(divisor);
  return scaledHeight;

}

/**
*Function to temporarily return the user to the home page
*
**/
function backButton(){
  goTo("index.html");
}
/*******************************************************************************
 * Loads the emoji slider (second panel) after a feeling is chosen by the user.
 * @param {String} emotion A string representing the emoji selected on the emoji
 * grid (first panel).
 * @param {Number} index A number representing the emojis index on the 3 x 3
 * grid.
 ******************************************************************************/
function loadSlider(emotion, index){

  showHide("emojiWrap"); // EmojiWrap hidden
  $("#main-p").html("How intense is your feeling today?"); // Replacing text for slider


  $('#emojie-drop').css("height", "65vh"); // Setting a dynamic height for the emoji dropdown for slider part
  $('#emojie-drop').css("max-height", "65vh");
  $('#emojie-drop').css("overflow", "unset");
  $('#main-p').css("margin-bottom", "13%"); // Setting feeling p tag to have desired margin for slider container.

  $('.rangeslider--vertical').css("height", (getSliderContainerScaledHeight(6) * 4)); // Setting height of vertical slider to 4/6 of emoji dropdown height.
  $('#wrapper').css("height", getSliderContainerScaledHeight(6)); // Setting height of vertical slider to 4/6 of emoji dropdown height.
  $('#navButtons').css("height", getSliderContainerScaledHeight(6)); // Setting height of vertical slider to 4/6 of emoji dropdown height.

  showHide("emojiSliderPopup"); //emojiSliderPopup show

  setEmojiHandle(emotion);

  // Changing height of slider div to fill screen, doing this to avoid changing
  // the height of each emoji screen.
  $("body").height($(window).height());

}

function setEmojiHandle(emotion){
  $(".rangeslider__handle").css("background-image","url(img/emojis/" + emotion + ".png)");
}

/*******************************************************************************
  * A function to dynamically set css values for Index.html
 ******************************************************************************/
function initialiseIndexCSS(){
  var screenHeight = $(window).height();
  //Gets value of navbar height as a string and uses regex to eliminate "px"
  var difference = $(".navbar-header").css("height").replace(/[^-\d\.]/g, '');
  var paddingValue = (screenHeight * 0.015) + parseInt(difference);

  $("body").css("padding-top", paddingValue.toString() + "px");

  //Sets the size of the glyphicon-menu-hamburger to be in scale for all devices
  var perfectSize = 170; // Nexus 5 prefered font size in %
  var perfectWidth = 412; //Nexus 5x width
  var selectedWidth = $(window).width(); // Device width
  var difference = selectedWidth / perfectWidth; // Difference between Nexus and device width
  var newSize = perfectSize * difference; // New font size in %
  $(".glyphicon-menu-hamburger").css("font-size", newSize + "%");
}

/*******************************************************************************
  Onload function that calls other functions on index.html
 ******************************************************************************/
function indexOnLoad(){

  loadEmojiSelection();
  initialiseIndexCSS();

}
