/*******************************************************************************
 * A function executed on refresh of page and actions, it's purpose is to customise
 * the aesthetics of the rangeSlider for the app: OpenUp.
 * @author W J Reynolds
 ******************************************************************************/
$(function() {
  sliderOuputCustomisation();

});

/*******************************************************************************
 *
 ******************************************************************************/
function sliderOuputCustomisation(){
  var output = document.querySelectorAll('output')[0];

  // Function executing on changes to the input (rangeslider).
  $(document).on('input', 'input[type="range"]', function(e) {
    var intensity = e.currentTarget.value; //The input selected by user

    // Depending on the intensity selected will determine the background colours
    // of the slider and the name of the intensity.
    switch (intensity) {
      case "2":
        $(".rangeslider__fill").css('background-color', '#B2FF59');
        $(".rangeslider").css('background-color', '#e6e6e6');
        output.innerHTML = "Fairly: " + intensity;
        break;
      case "3":
        $(".rangeslider__fill").css('background-color', '#FFFF00');
        $(".rangeslider").css('background-color', '#e6e6e6');
        output.innerHTML = "Moderately: " + intensity;
        break;
      case "4":
        $(".rangeslider__fill").css('background-color', '#FFA000');
        $(".rangeslider").css('background-color', '#e6e6e6');
        output.innerHTML = "Very: " + intensity;
        break;
      case "5":
        $(".rangeslider__fill").css('background-color', '#FF3D00');
        $(".rangeslider").css('background-color', '#FF3D00');
        output.innerHTML = "Extremely: " + intensity;
        break;
      default:
          $(".rangeslider").css('background-color', '#e6e6e6');
          output.innerHTML = "Slightly: " + intensity;
      }

  });


  /**
   * Keep this!!! Allows customisation of the rangeslider
   */
  $('input[type=range]').rangeslider({
      polyfill: false
    });

}
/*******************************************************************************
 * A set function that adjusts the rangesliders smiley handle to the name of the
 * smiley passed as a parameter.
 * @param {string} smileyName A string that represents the smiley that is required
 * to be set as the handle.
 ******************************************************************************/
function setSmiley(smileyName){
  $(".rangeslider__handle").css("background-image", "url(\"../www/res/icon/emojis/" + smileyName + ".png\")");
}
