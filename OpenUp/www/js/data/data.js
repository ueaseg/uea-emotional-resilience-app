/*
  saveConsent
  Function that looks to the consent form, retrieves values and writes outpt
  to 00-control.csv.
*/
function saveConsent(e) {

  //Variables for the control file
  var userID = document.getElementById("userID").value;
  var userEmail = document.getElementById("userEmail").value;
  var consent = document.getElementById('consent').checked;

  //valiate he fields here

  //specify the regex - only interested in uea.ac.uk
  var re = new RegExp("[a-zA-Z0-9.]+@uea\.ac\.uk");
  var result = re.test(userEmail);

  //Test value of result - if False then show the email error
  if (!result) {
    console.log("Email failed Regex validation");
    //Make error text visible
    $(".emailError").show();

  } else {

    console.log("Email is acceptable");

    var dataString = "";

    //Datastring for control
    dataString = consent + "," + userID + "," + userEmail;
    writeData("00-control.csv", dataString, false);
    $("#userID").prop("readonly", true);

  }

}

/*
  saveDemographics
  Function which takes demographic form inputs, validates (returning feedback where necessary).
  Data will be written to 01-demographics.csv
*/
function saveDemographics(e) {

  var isValid = "true";

  //validate gender input
  if (document.getElementById("gender").checkValidity() == false) {

        document.getElementById("validationMsg-gender").innerHTML = document.getElementById("gender").validationMessage;
        isValid = "false";

  } else {

    document.getElementById("validationMsg-gender").innerHTML = "";
    var gender = document.getElementById("gender").value;

  }

  //validate age input
  if (document.getElementById("age").checkValidity() == false) {

        document.getElementById("validationMsg-age").innerHTML = document.getElementById("age").validationMessage;
        isValid = "false";

  } else {

    document.getElementById("validationMsg-age").innerHTML = "";
    var age = document.getElementById("age").value;

  }

  //validate ethnicity input
  if (document.getElementById("ethnicity").checkValidity() == false) {

        document.getElementById("validationMsg-ethnicity").innerHTML = document.getElementById("ethnicity").validationMessage;
        isValid = "false";

  } else {

    document.getElementById("validationMsg-ethnicity").innerHTML = "";
    var ethnicity = document.getElementById("ethnicity").value;

  }

  //validate institution input
  if (document.getElementById("institution").checkValidity() == false) {

        document.getElementById("validationMsg-institution").innerHTML = document.getElementById("institution").validationMessage;
        isValid = "false";

  } else {

    document.getElementById("validationMsg-institution").innerHTML = "";
    var institution = document.getElementById("institution").value;

  }

  //validate start year input
  if (document.getElementById("startYear").checkValidity() == false) {

        document.getElementById("validationMsg-startYear").innerHTML = document.getElementById("startYear").validationMessage;
        isValid = "false";

  } else {

    document.getElementById("validationMsg-startYear").innerHTML = "";
    var startYear = document.getElementById("startYear").value;

  }

  //validate faculty input
  if (document.getElementById("faculty").checkValidity() == false) {

        document.getElementById("validationMsg-faculty").innerHTML = document.getElementById("faculty").validationMessage;
        isValid = "false";

  } else {

    document.getElementById("validationMsg-faculty").innerHTML = "";
    var faculty = document.getElementById("faculty").value;

  }

  //validate uni year input
  if (document.getElementById("uniYear").checkValidity() == false) {

        document.getElementById("validationMsg-uniYear").innerHTML = document.getElementById("uniYear").validationMessage;
        isValid = "false";

  } else {

    document.getElementById("validationMsg-uniYear").innerHTML = "";
    var uniYear = document.getElementById("uniYear").value;

  }

  //validate home-int input
  if (document.getElementById("home-int").checkValidity() == false) {

        document.getElementById("validationMsg-homeInt").innerHTML = document.getElementById("home-int").validationMessage;
        isValid = "false";

  } else {

    document.getElementById("validationMsg-homeInt").innerHTML = "";
    var homeInt = document.getElementById("home-int").value;

  }

  //validate disabiity input
  if (document.getElementById("disability").checkValidity() == false) {

        document.getElementById("validationMsg-disability").innerHTML = document.getElementById("disability").validationMessage;
        isValid = "false";

  } else {
    document.getElementById("validationMsg-disability").innerHTML = "";
    var disability = document.getElementById("disability").value;

  }

  //Check the isValud variable - if true then go ahead and prepare the datastring
  if (isValid == "true") {
    getUserID().then(function(userID){

      //Define the CSV string
      dataString = userID + "," + getDate()  + "," + getTime() + "," + gender + "," + age + "," + ethnicity + "," + institution + "," + startYear + "," + faculty + "," + uniYear + "," + homeInt + "," + disability  + "\n";

      //Write the data
      writeData("01-demographics.csv", dataString, false);

      alert("Details Saved Successfully");

    });
  } else {

    alert("Cannot save details - please check fields and try again");

  }

}
/*
  saveEmoji
  Function to take emoji choices, reasons and intensities and write them to file
  02-emotions.csv
*/
function saveEmoji(emoji, intensity, reasons, url) {

  var datastring = "";

    getUserID().then(function(userID) {

      reasonString = ''

      reasons.forEach(function(reason){
        reasonString += ',' + reason;
      });

      // Remove the comma from the beginning of the reason string.
      reasonString = reasonString.substr(1);

    //Prepar the CSV string
    dataString = userID + "," + getDate() + "," + getTime() + "," + emoji + "," + intensity + ",\"" + reasonString + "\"\n";

    //Write the data
    writeData("02-emotions.csv", dataString, true).then(function(){
      goTo(url);
    });

  });

}
/*
  saveCardViewed
  Function that writes to file 03-cardsViewed.csv each time a support card is
  expanded
*/
function saveCardViewed(cardId) {

  var datastring = "";

  getUserID().then(function(userID) {

  //Prepare the CSV string
  dataString = userID + "," + getDate() + "," + getTime() + "," + cardId + "\n";

  //Write the data
  writeData("03-cardsViewed.csv", dataString, true);

  });

}

/*
  saveAction
  Function to which writes to 04-actionTaken.csv each time the user selects a
  button within an expanded support card
*/
function saveAction(cardId, action, url) {

  var dataString = "";

  getUserID().then(function(userID) {

    //Prepare the CSV string
    dataString = userID + "," + getDate() + "," + getTime() + "," + cardId + "," + action + "\n";

    //Write the data
    writeData("04-actionTaken.csv", dataString, true).then(
      function(){
        if(url){
          window.location.href=url;
        }
      }
    );

  });

}

/*
  isFirstLoad
  Function to determine if launch of App is first load.
  If firstLoad.csv exists, then the app has been launched previously.
  Else create the file and write the date and time to it whilst redirecting
  the user to the information page regarding the app and the research
*/
function isFirstLoad() {

  window.resolveLocalFileSystemURL(cordova.file.externalDataDirectory, function (dirEntry) {

    //Attempt to get the file firstLoad.csv
    dirEntry.getFile("firstLoad.csv", {
      create: false,
      exclusive: false
    }, function (fileEntry) {

      //If firstLoad.csv exists, then do nothing
      console.log("Not first load");

    },function(err){
        var dataFile = getDate() + "," + getTime();
        console.log("First Load Detected - Redirected");
        writeData ("firstLoad.csv", dataFile, false).then(function(){
          window.location = "dataInfo.html";
        });

    });

  });

}

/*
  readData
  General funtion that reads the contents of a given csv file, providing an
  array data items for which a function calling this function should know how
  to deal with
*/
function readData(file) {

  return new Promise((resolve, reject) => {

  window.resolveLocalFileSystemURL(cordova.file.externalDataDirectory, function (dirEntry) {

    //Attempt to get the file
    dirEntry.getFile(file, {
      create: false,
      exclusive: false
    }, function (fileEntry) {

      //If file exists - go in and get the contents
      console.log(file + " Exists");

      //Can we enter the file?
      fileEntry.file(function(file){

        //Define new fileReader object
        var reader = new FileReader();
        reader.onloadend = function(evt) {

          resolve(evt.target.result);

        };

        reader.readAsText(file);

      }, function(evt){

        console.log("Error code: " + evt.target.error.code);
        reject();

      });

    },function(err){

        console.log(file + " not found - carry on");
        reject();

    });

  });

  });

}

/*
  write Data
  General function that writes a provided dataString to a given file.
  The dataString and filename are provided by the calling function.
*/
function writeData(file, dataString, append) {

  return new Promise((resolve, reject) => {

    window.resolveLocalFileSystemURL(cordova.file.externalDataDirectory, function (dirEntry) {

        dirEntry.getFile(file, {
            create: true,
            exclusive: false
        }, function (fileEntry) {

            console.log("Found file...Starting Writing");

            fileEntry.createWriter(function (fileWriter) {

              // Move to the end of the file so that the content that is written is
              // appended to the file.

              if (append){

                fileWriter.seek(fileWriter.length);

              }

              // When the file is finished writing the indicate this.
              fileWriter.onwriteend = function() {
                  console.log("Successful file write...");
              };

              // If an error occurs then print the error to the console.
              fileWriter.onerror = function (e) {
                  console.log("Failed file write: " + e.toString());
              };

              // Write the data - leave new line at end.
              fileWriter.write(dataString);

              console.log("Done Writing");

              resolve();


      })},

      function(err){
          console.log("Error opening file");
      });

    });

  });

}

/*
  getDate
  Simple function to return the current date in dd/mm/yyyy format from the
  system
*/
function getDate() {

  var today = new Date();
  var dd = today.getDate();
  var mm = today.getMonth()+1; //January is 0!
  var yyyy = today.getFullYear();

  //prepend a 0 if day is 0 - 9
  if(dd<10) {
      dd = '0'+dd
  }

  //prepend a 0 if month is 0 - 9
  if(mm<10) {
      mm = '0'+mm
    }

  return mm + '/' + dd + '/' + yyyy;
}

/*
  getTime
  Simple function to return the current time in hh:mm format from the system
*/
function getTime() {

  var today = new Date();
  var h = today.getHours();
  var m = today.getMinutes();

  //prepend a 0 if hour is 0 - 9
  if (h < 10) {
    h = '0'+h;
  }

  //prepend a 0 if minutes are 0 - 9
  if (m < 10) {
    m = '0'+m;
  }

  return h + ":" + m;

}

//Function to show form userID and email when user checks the consent tickbox (takes the necessary div argument)
function showFormGroup(div) {

    var chboxs = document.getElementById("consent");

    if(chboxs.checked == true) {
      $(div).show();
    } else {
      $(div).hide();
    }

}

/*
  getUserID
  Function to retrieve the userID from the 00-control.csv file which is then
  used by other functions writing data
*/
function getUserID() {
  return new Promise((resolve, reject) => {
    try{
      readData("00-control.csv").then(function(text){
        var csvData = text.split(",");
        resolve(csvData[1]);
      });
    } catch(e){
      reject(e);
    }
  });
}

/*
  getDemographics
  Function to retrive contents from 01-demographics.csv if it exists and
  populate the demographics form with the data which is split out.
*/
function getDemographics() {
  return new Promise((resolve, reject) => {
    try{
      readData("01-demographics.csv").then(function(text){
        console.log("demographics.csv exists - attempting to access data");

        //Split data out into an array using , delimeter
        var csvData = text.trim().split(",");

        //Set the values of the respective form fields
        document.getElementById("gender").value = csvData[3];
        document.getElementById("age").value = csvData[4];
        document.getElementById("ethnicity").value = csvData[5];
        document.getElementById("institution").value = csvData[6];
        document.getElementById("startYear").value = csvData[7];
        document.getElementById("faculty").value = csvData[8];
        document.getElementById("uniYear").value = csvData[9];
        document.getElementById("home-int").value = csvData[10];
        document.getElementById("disability").value = csvData[11];

        resolve();
      });
    } catch(e){
      reject(e);
    }
  });
}
