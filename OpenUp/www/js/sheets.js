/*
Query the sheet for a data update - trigger a message if an update is available,
allowing the user to update or ignore it.
*/
function checkForUpdate()
{
  window.resolveLocalFileSystemURL(cordova.file.externalDataDirectory, function (dirEntry) {

    //Attempt to get update file
    dirEntry.getFile("update.csv", {
      create: false,
      exclusive: false
    }, function (fileEntry) {

      //If update.csv exists, then do nothing
      console.log("Update.csv exists");

      //Read date written in file
      readData("update.csv").then(function(text) {
        var data = text.split(",");

        console.log("Read from csv: " + data);
        //Spit out an alert to the user to notify that data is already up to date
        alert("Data already up to date.");

        if(data > getUpdateCell()) {
          console.log("Update available");
          performUpdate();
        }
      });

    }, function(err) {

      //File doesn't exist, create new update.csv
      console.log("File does not exist");

      console.log("Date of last update: " + getUpdateCell());
      writeData("update.csv", getUpdateCell(), false);

      performUpdate();
    });
  });
}

function performUpdate() {

  function onConfirm(btnIndex) {
    console.log("You selected button " + btnIndex);

    //1 - update
    //2 - do not update

    if(btnIndex == 1) {
      writeData('services.json', importJson(), false);
      console.log("UPDATED");
      writeData('update.csv', getUpdateCell(), false);
    } else {
      console.log("Chose not to update")
    }
  }


  navigator.notification.confirm(
    'Update Services List?',  // Dialog message
    onConfirm,                // Callback function
    'Update?',                // Title of dialog
    ['Update', 'Exit']        // Button Labels
  );

}

function getUpdateCell()
{
  var link = getSheetLink('N2');

  var data = getXmlData(link);

  var file = toJson(data);

  function toDate(str)
  {
    day = new Date();


    var t = str.split('/');
    var t2 = t[2].split(' ');
    var t3 = t2[1].split(' ');
    var t4 = t3[0].split(':');

    day.setDate(t[0]);
    day.setMonth(t[1]);
    day.setFullYear(t2[0]);
    day.setHours(t4[0]);
    day.setMinutes(t4[1]);
    day.setSeconds(t4[2]);

    console.log('Last updated: ' + day);

    return day;
  }

  return toDate(file[0][0]);
}

function toJson(data)
{
  var j = JSON.parse(data);
  var file = j.valueRanges[0].values;

  console.log(file);

  return file;
}

function getSheetLink(range)
{
  var sheetId = '1ipfHM9GD6sB6q8tBmqa9r4Y9AQCZLBkdZ09QWOty4Ks';
  var rangeOfCells;

  if(range)
    rangeOfCells = 'Sheet1!' + range;
  else
    rangeOfCells = 'Sheet1!A2:L39';

  var apiKey = 'AIzaSyACE26nhuL-JFsxSERxXDCaLgMJPZ7KS2s';

  var link =  'https://sheets.googleapis.com/v4/spreadsheets/' + sheetId +
              '/values:batchGet?ranges=' + rangeOfCells + '&key=' + apiKey;

  return link;
}

function getXmlData(link)
{
  var xmlHttp = new XMLHttpRequest();
  xmlHttp.open("GET", link, false);
  xmlHttp.send(null);

  var data = xmlHttp.responseText;

  return data;
}

/*
Import the Sheet data, and output as JSON
*/
function importJson()
{
  var link = getSheetLink('');

  console.log(link);

  var data = getXmlData(link);

  var file = toJson(data);

  return file;
}

/*
Function to write sheet data to an offline JSON file
*/
function writeJson(file) {

  var json = importJson();

  return new Promise((resolve, reject) => {

    window.resolveLocalFileSystemURL(cordova.file.externalDataDirectory, function (dirEntry) {

      dirEntry.getFile(file, {
        create: true,
        exclusive: false
      }, function (fileEntry) {

        console.log("File found...writing");

        fileEntry.createWriter(function (fileWriter) {

          fileWriter.onwriteend = function() {
            console.log("File written");
          };

          fileWriter.onerror = function(e) {
            console.log("Failed write: " + e.toString());
          };

          fileWriter.write(json);

          console.log("Done writing");

          resolve();
      })},

        function(err){
          console.log("Error opening file");
        });
    });
  });
}

function tryServices(callback) {

  window.resolveLocalFileSystemURL(cordova.file.externalDataDirectory, function (dirEntry) {

    try{

      //Attempt to get services
      dirEntry.getFile("services.json", {
        create: false,
        exclusive: false
      }, function (fileEntry) {

        console.log("found services");
        //Get the services.json fileEntry
        //var servicesFile = getServiceJSON();
        getServiceJSON(callback);

      }, function(err) {
        console.log("no services found");
        noServicesModal();

      });

    } catch(e) {
      //File doesn't exist, create and update services.json
      console.log("file cound not be downloaded");
      navigator.notification.alert("File could not be downloaded");
    }

  });

}

function getServiceJSON(callback) {
  readData("services.json").then(function(x) {
    var services = JSON.parse(x);
    callback(services);
  });
}

/*
  Produce model informing user that no services data exists - they should
  download it to continue
*/
function noServicesModal() {

  function onConfirm(btnIndex) {
    console.log("You selected button " + btnIndex);

    //1 - update
    //2 - do not update

    if(btnIndex == 1) {
      writeData('services.json', importJson(), false).then(function(){

        console.log("UPDATED");
        writeData('update.csv', getUpdateCell(), false).then(function(){
          window.location.reload();
        });

      });


    } else {
      console.log("Chose not to update");
    }
  }


  navigator.notification.confirm(
    'No Services found, download services?',  // Dialog message
    onConfirm,                                // Callback function
    'Services',                               // Title of dialog
    ['Download', 'Exit']                      // Button Labels
  );
}
